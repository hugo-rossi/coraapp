package io.github.hrossi.cora.data.repository.bank

import androidx.lifecycle.LiveData
import io.github.hrossi.cora.data.local.entity.Bank

interface BankRepository {

    fun selectAll(): LiveData<List<Bank>>

}