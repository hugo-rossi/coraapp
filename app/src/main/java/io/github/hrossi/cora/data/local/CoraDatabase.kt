package io.github.hrossi.cora.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import io.github.hrossi.cora.data.local.dao.BankDAO
import io.github.hrossi.cora.data.local.dao.ContactDAO
import io.github.hrossi.cora.data.local.entity.Bank
import io.github.hrossi.cora.data.local.entity.Contact

@Database(
    entities = [
        Bank::class,
        Contact::class
    ],
    version = 1
)
abstract class CoraDatabase : RoomDatabase() {

    abstract fun bankDAO(): BankDAO

    abstract fun contactDAO(): ContactDAO

}