package io.github.hrossi.cora.presentation.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import io.github.hrossi.cora.R
import io.github.hrossi.cora.common.util.Mask
import io.github.hrossi.cora.databinding.CustomInputViewBinding

class InputView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    private val viewModel = InputViewViewModel()

    private val viewBinding: CustomInputViewBinding = DataBindingUtil.inflate(
        LayoutInflater.from(context), R.layout.custom_input_view, this, true
    )

    init {
        viewBinding.viewModelInView = viewModel

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.InputView)

        orientation = VERTICAL

        viewBinding.textViewTitle.text = attributes.getString(R.styleable.InputView_title)
        viewBinding.editText.hint = attributes.getString(R.styleable.InputView_hint)
        viewBinding.editText.inputType = attributes.getInteger(R.styleable.InputView_android_inputType, -1)

        attributes.getString(R.styleable.InputView_mask)?.let { mask ->
            viewBinding.editText.addTextChangedListener(Mask.mask(mask, viewBinding.editText))
        }

        attributes.getString(R.styleable.InputView_regex)?.let { regex ->
            viewModel.regex = regex
        }

        attributes.recycle()
    }

    fun getCurrentText(): String? {
        return viewModel.text.value
    }

    fun isValid(): Boolean {
        return viewModel.isValid.value!!
    }

    fun setValid(boolean: Boolean) {
        viewModel.isValid.postValue(boolean)
    }

    fun addListener(function: () -> Unit) {
        viewBinding.editText.addTextChangedListener(afterTextChanged = {
            function.invoke()
        })
    }

}

@BindingAdapter(value = ["isValidAttrChanged"])
fun setListener(inputView: InputView, listener: InverseBindingListener) {
    inputView.addListener {
        listener.onChange()
    }
}

@BindingAdapter("isValid")
fun setRealValue(view: InputView, value: Boolean) {
    view.setValid(value)
}

@InverseBindingAdapter(attribute = "isValid")
fun isRealValue(view: InputView): Boolean {
    return view.isValid()
}