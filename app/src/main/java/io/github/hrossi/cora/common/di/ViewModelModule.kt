package io.github.hrossi.cora.common.di

import io.github.hrossi.cora.presentation.contact.list.ContactListViewModel
import io.github.hrossi.cora.presentation.contact.neww.NewContactViewModel
import io.github.hrossi.cora.presentation.custom.InputViewViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        ContactListViewModel(get())
    }

    viewModel {
        NewContactViewModel(get(), get())
    }

    viewModel {
        InputViewViewModel()
    }

}