package io.github.hrossi.cora.data.repository.contact

import androidx.lifecycle.LiveData
import io.github.hrossi.cora.data.local.dao.ContactDAO
import io.github.hrossi.cora.data.local.entity.Contact

class ContactRepositoryImpl(private val dao: ContactDAO) : ContactRepository {

    override suspend fun insert(contact: Contact) {
        dao.insert(contact)
    }

    override fun selectAll(): LiveData<List<Contact>> {
        return dao.selectAll()
    }

}