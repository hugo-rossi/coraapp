package io.github.hrossi.cora.presentation.contact.neww

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.github.hrossi.cora.data.local.entity.Bank
import io.github.hrossi.cora.data.local.entity.Contact
import io.github.hrossi.cora.data.repository.bank.BankRepository
import io.github.hrossi.cora.data.repository.contact.ContactRepository
import kotlinx.coroutines.launch
import java.util.*

class NewContactViewModel(
    bankRepository: BankRepository,
    private val contactRepository: ContactRepository
) : ViewModel() {

    val banks: LiveData<List<Bank>> = bankRepository.selectAll()

    var isValidCpf = ObservableField<Boolean>()

    var isValidName = ObservableField<Boolean>()
    val isValidAgency = ObservableField<Boolean>()
    val isValidAccount = ObservableField<Boolean>()

    fun saveNewContact(cpf: String, name: String, bank: String, agency: String, account: String) {
        viewModelScope.launch {
            contactRepository.insert(
                Contact(
                    cpf = cpf,
                    name = name,
                    bank = bank,
                    agency = agency,
                    account = account
                )
            )
        }
    }

}
