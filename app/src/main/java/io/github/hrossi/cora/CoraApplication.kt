package io.github.hrossi.cora

import android.app.Application
import io.github.hrossi.cora.common.di.databaseModule
import io.github.hrossi.cora.common.di.repositoryModule
import io.github.hrossi.cora.common.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CoraApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            // declare used Android context
            androidContext(this@CoraApplication)
            // declare modules
            modules(
                listOf(
                    databaseModule,
                    repositoryModule,
                    viewModelModule
                )
            )
        }
    }

}