package io.github.hrossi.cora.data.repository.contact

import androidx.lifecycle.LiveData
import io.github.hrossi.cora.data.local.entity.Contact

interface ContactRepository {

    suspend fun insert(contact: Contact)

    fun selectAll(): LiveData<List<Contact>>

}