package io.github.hrossi.cora.common.di

import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import io.github.hrossi.cora.data.local.CoraDatabase
import org.koin.dsl.module
import java.util.*

val databaseModule = module {

    single {
        Room.databaseBuilder(get(), CoraDatabase::class.java, "CoraApp.db")
            .addCallback(PopulateDatabaseCallback())
            .build()
    }

    factory {
        (get() as CoraDatabase).bankDAO()
    }

    factory {
        (get() as CoraDatabase).contactDAO()
    }

}

private class PopulateDatabaseCallback : RoomDatabase.Callback() {

    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)
        db.execSQL("Insert into Contact values ('${UUID.randomUUID()}', '123.456.789-09', 'Angel Flores', '001', '0001','1234', false)")
        db.execSQL("Insert into Contact values ('${UUID.randomUUID()}', '987.654.321-00', 'Esther Webb', '033', '0001','12345', false)")

        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '001', 'Banco do Brasil')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '033', 'Santander')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '237', 'Bradesco')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '302', 'Itaú')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '422', 'Safra')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '422', 'Safra')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '422', 'Safra')")
        db.execSQL("Insert into Bank values ('${UUID.randomUUID()}', '422', 'Safra')")
    }

}