package io.github.hrossi.cora.presentation.contact.neww.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import io.github.hrossi.cora.R
import io.github.hrossi.cora.databinding.FragmentNewContactAccountBinding
import io.github.hrossi.cora.presentation.contact.neww.NewContactViewModel
import kotlinx.android.synthetic.main.fragment_new_contact_account.*
import org.koin.android.viewmodel.ext.android.viewModel

class NewContactAccountFragment : Fragment() {

    private val viewModel: NewContactViewModel by viewModel()

    private val args: NewContactAccountFragmentArgs by navArgs()

    private lateinit var viewBinding: FragmentNewContactAccountBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_contact_account, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.newContactViewModel = viewModel

        setupListeners()
    }

    private fun setupListeners() {
        buttonNext.setOnClickListener {
            viewModel.saveNewContact(args.cpf, args.name, args.bank, args.agency, inputView.getCurrentText().toString())
            it.findNavController().navigate(R.id.action_newContactAccountFragment_to_newContactSuccessFragment)
        }
    }

}