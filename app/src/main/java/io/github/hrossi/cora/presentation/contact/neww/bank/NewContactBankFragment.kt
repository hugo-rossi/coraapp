package io.github.hrossi.cora.presentation.contact.neww.bank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.github.hrossi.cora.R
import io.github.hrossi.cora.data.local.entity.Bank
import io.github.hrossi.cora.databinding.FragmentNewContactBankBinding
import io.github.hrossi.cora.presentation.contact.neww.NewContactViewModel
import io.github.hrossi.cora.presentation.contact.neww.agency.NewContactAgencyFragmentArgs
import io.github.hrossi.cora.presentation.contact.neww.bank.adapter.BankChooserAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class NewContactBankFragment : Fragment() {

    private val viewModel: NewContactViewModel by viewModel()

    private val args: NewContactBankFragmentArgs by navArgs()

    private lateinit var viewBinding: FragmentNewContactBankBinding

    private val adapter by lazy {
        BankChooserAdapter(::onClick)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_contact_bank, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.newContactViewModel = viewModel

        setupAdapter()
        setupObservers()
    }

    private fun setupAdapter() {
        viewBinding.recyclerViewBanks.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.banks.observe(requireActivity(), Observer { banks ->
            banks?.let { adapter.setData(it) }
        })
    }

    private fun onClick(bank: Bank) {
        findNavController().navigate(
            NewContactBankFragmentDirections.actionNewContactBankFragmentToNewContactAgencyFragment(
                args.cpf,
                args.name,
                bank.code
            )
        )
    }
}
