package io.github.hrossi.cora.presentation.contact.neww.bank.adapter

import androidx.recyclerview.widget.RecyclerView
import io.github.hrossi.cora.data.local.entity.Bank
import io.github.hrossi.cora.databinding.ListItemBankBinding

class BankViewHolder(val binding: ListItemBankBinding) : RecyclerView.ViewHolder(binding.root) {

    fun format(
        bank: Bank,
        listener: (Bank) -> Unit
    ) {
        with(binding) {
            textViewBankName.text = bank.name
            textViewBankNumber.text = bank.code
            root.setOnClickListener {
                listener.invoke(bank)
            }
        }
    }

}