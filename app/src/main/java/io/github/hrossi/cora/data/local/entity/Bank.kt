package io.github.hrossi.cora.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Bank(
    @PrimaryKey
    val id: String,
    val code: String,
    val name: String
)