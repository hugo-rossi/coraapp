package io.github.hrossi.cora.data.repository.bank

import androidx.lifecycle.LiveData
import io.github.hrossi.cora.data.local.dao.BankDAO
import io.github.hrossi.cora.data.local.entity.Bank

class BankRepositoryImpl(private val bankDao: BankDAO) : BankRepository {

    override fun selectAll(): LiveData<List<Bank>> {
        return bankDao.selectAll()
    }

}