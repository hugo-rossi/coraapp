package io.github.hrossi.cora.presentation.contact.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.github.hrossi.cora.data.local.entity.Contact
import io.github.hrossi.cora.data.repository.contact.ContactRepository
import kotlinx.coroutines.launch

class ContactListViewModel(private val repository: ContactRepository) : ViewModel() {

    val contacts: LiveData<List<Contact>> = repository.selectAll()

}