package io.github.hrossi.cora.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.github.hrossi.cora.data.local.entity.Bank

@Dao
interface BankDAO {

    @Insert
    suspend fun insert(bank: Bank)

    @Query("SELECT * FROM Bank")
    fun selectAll(): LiveData<List<Bank>>

}