package io.github.hrossi.cora.presentation.contact.neww.bank.adapter

import androidx.recyclerview.widget.RecyclerView
import io.github.hrossi.cora.databinding.ListItemHeaderBinding

class HeaderViewHolder(val binding: ListItemHeaderBinding) : RecyclerView.ViewHolder(binding.root) {

    fun format(header: String) {
        with(binding) {
            textViewHeader.text = header
        }
    }

}