package io.github.hrossi.cora.common.di

import io.github.hrossi.cora.data.repository.bank.BankRepository
import io.github.hrossi.cora.data.repository.bank.BankRepositoryImpl
import io.github.hrossi.cora.data.repository.contact.ContactRepository
import io.github.hrossi.cora.data.repository.contact.ContactRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {

    factory<ContactRepository> {
        ContactRepositoryImpl(get())
    }

    factory<BankRepository> {
        BankRepositoryImpl(get())
    }

}