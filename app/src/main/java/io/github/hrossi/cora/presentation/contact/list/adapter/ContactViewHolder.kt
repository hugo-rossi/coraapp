package io.github.hrossi.cora.presentation.contact.list.adapter

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.RecyclerView
import io.github.hrossi.cora.data.local.entity.Contact
import io.github.hrossi.cora.databinding.ListItemContactBinding

class ContactViewHolder(val binding: ListItemContactBinding) : RecyclerView.ViewHolder(binding.root) {

    fun format(
        item: Contact,
        xpto: (Contact) -> Unit
    ) {
        with(binding) {
            textViewContactName.text = item.name
            textViewContactCpf.text = item.cpf
            textViewChipNewContact.visibility = if (item.isNew) VISIBLE else GONE
            root.setOnClickListener { xpto.invoke(item) }
        }
    }

}