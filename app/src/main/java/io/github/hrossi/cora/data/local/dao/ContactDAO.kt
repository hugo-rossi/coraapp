package io.github.hrossi.cora.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.github.hrossi.cora.data.local.entity.Contact

@Dao
interface ContactDAO {

    @Insert
    suspend fun insert(contact: Contact)

    @Query("SELECT * FROM Contact")
    fun selectAll(): LiveData<List<Contact>>

}