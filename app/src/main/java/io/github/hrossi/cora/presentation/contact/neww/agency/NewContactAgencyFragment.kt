package io.github.hrossi.cora.presentation.contact.neww.agency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import io.github.hrossi.cora.R
import io.github.hrossi.cora.databinding.FragmentNewContactAgencyBinding
import io.github.hrossi.cora.presentation.contact.neww.NewContactViewModel
import kotlinx.android.synthetic.main.fragment_new_contact_agency.*
import org.koin.android.viewmodel.ext.android.viewModel

class NewContactAgencyFragment : Fragment() {

    private val viewModel: NewContactViewModel by viewModel()

    private val args: NewContactAgencyFragmentArgs by navArgs()

    private lateinit var viewBinding: FragmentNewContactAgencyBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_contact_agency, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.newContactViewModel = viewModel

        setupListeners()
    }

    private fun setupListeners() {
        buttonNext.setOnClickListener {
            it.findNavController().navigate(
                NewContactAgencyFragmentDirections.actionNewContactAgencyFragmentToNewContactAccountFragment(
                    args.cpf,
                    args.name,
                    args.bank,
                    inputView.getCurrentText().toString()
                )
            )
        }
    }

}