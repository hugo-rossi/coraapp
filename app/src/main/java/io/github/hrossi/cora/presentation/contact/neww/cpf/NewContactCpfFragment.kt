package io.github.hrossi.cora.presentation.contact.neww.cpf

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import io.github.hrossi.cora.R
import io.github.hrossi.cora.databinding.FragmentNewContactCpfBinding
import io.github.hrossi.cora.presentation.contact.neww.NewContactViewModel
import kotlinx.android.synthetic.main.fragment_new_contact_cpf.*
import org.koin.android.viewmodel.ext.android.viewModel

class NewContactCpfFragment : Fragment() {

    private val viewModel: NewContactViewModel by viewModel()

    private lateinit var viewBinding: FragmentNewContactCpfBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_contact_cpf, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.newContactViewModel = viewModel

        setupListeners()
    }

    private fun setupListeners() {
        buttonNext.setOnClickListener {
            it.findNavController().navigate(
                NewContactCpfFragmentDirections.actionNewContactCpfFragmentToNewContactNameFragment(
                    inputView.getCurrentText().toString()
                )
            )
        }
    }

}