package io.github.hrossi.cora.presentation.custom

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InputViewViewModel : ViewModel() {

    var regex: String? = null

    val text = MutableLiveData<String>()

    val isValid: MutableLiveData<Boolean>
        get() {
            val valid = regex?.let {
                Regex(it).matches(text.value.toString())
            } ?: text.value?.isNotBlank() ?: false

            return MutableLiveData(valid)
        }
}
