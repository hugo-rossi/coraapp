package io.github.hrossi.cora.presentation.contact.list

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import io.github.hrossi.cora.R
import io.github.hrossi.cora.data.local.entity.Contact
import io.github.hrossi.cora.databinding.ActivityContactListBinding
import io.github.hrossi.cora.presentation.contact.list.adapter.ContactAdapter
import io.github.hrossi.cora.presentation.contact.neww.NewContactActivity
import kotlinx.android.synthetic.main.activity_contact_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class ContactListActivity : AppCompatActivity() {

    private val viewModel: ContactListViewModel by viewModel()

    private lateinit var viewBinding: ActivityContactListBinding

    private val adapter by lazy {
        ContactAdapter(::onClick)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact_list)

        setupToolbar()
        setupAdapter()
        setupObservers()
        setupListeners()
    }

    private fun setupListeners() {
        textViewAddContact.setOnClickListener {
            startActivity(NewContactActivity.createIntent(this))
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
    }

    private fun setupAdapter() {
        viewBinding.recyclerViewContacts.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.contacts.observe(this, Observer { contacts ->
            contacts?.let { adapter.setData(it) }
        })
    }

    private fun onClick(contact: Contact) {
        Toast.makeText(this, "OnClick -> ${contact.name}", Toast.LENGTH_SHORT).show()
    }

}
