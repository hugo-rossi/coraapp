package io.github.hrossi.cora.presentation.contact.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.github.hrossi.cora.data.local.entity.Contact
import io.github.hrossi.cora.databinding.ListItemContactBinding
import io.github.hrossi.cora.databinding.ListItemHeaderBinding
import io.github.hrossi.cora.presentation.contact.neww.bank.adapter.BankChooserAdapter
import io.github.hrossi.cora.presentation.contact.list.adapter.ContactAdapter.ContactAdapterViewType.HEADER
import io.github.hrossi.cora.presentation.contact.list.adapter.ContactAdapter.ContactAdapterViewType.ITEM

class ContactAdapter(val listener : (Contact) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: List<Any> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            HEADER -> {
                val binding = ListItemHeaderBinding.inflate(inflater, parent, false)
                HeaderViewHolder(binding)
            }
            ITEM -> {
                val binding = ListItemContactBinding.inflate(inflater, parent, false)
                ContactViewHolder(binding)
            }
            else -> throw NotImplementedError("Unknown View Type")
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val obj = list[position]

        when (getItemViewType(position)) {
            HEADER -> {
                (holder as HeaderViewHolder).format(obj.toString())
            }
            ITEM -> {
                (holder as ContactViewHolder).format(obj as Contact, listener)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (list[position]) {
            is String -> BankChooserAdapter.ContactAdapterViewType.HEADER
            else -> BankChooserAdapter.ContactAdapterViewType.ITEM
        }
    }

    internal fun setData(data: List<Any>) {
        val list = data.toMutableList()
        list.add(0, "Contatos Cadastrados")
        this.list = list
        notifyDataSetChanged()
    }

    object ContactAdapterViewType {
        const val HEADER = 11
        const val ITEM = 22
    }

}