package io.github.hrossi.cora.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Contact")
class Contact(
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    val cpf: String,
    val name: String,
    val bank: String,
    val agency: String,
    val account: String,
    val isNew: Boolean = true
)