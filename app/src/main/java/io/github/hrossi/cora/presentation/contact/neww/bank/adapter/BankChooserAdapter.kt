package io.github.hrossi.cora.presentation.contact.neww.bank.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.github.hrossi.cora.data.local.entity.Bank
import io.github.hrossi.cora.databinding.ListItemBankBinding
import io.github.hrossi.cora.databinding.ListItemHeaderBinding
import io.github.hrossi.cora.presentation.contact.neww.bank.adapter.BankChooserAdapter.ContactAdapterViewType.HEADER
import io.github.hrossi.cora.presentation.contact.neww.bank.adapter.BankChooserAdapter.ContactAdapterViewType.ITEM

class BankChooserAdapter(val listener : (Bank) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: List<Any> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            HEADER -> {
                val binding = ListItemHeaderBinding.inflate(inflater, parent, false)
                HeaderViewHolder(binding)
            }
            ITEM -> {
                val binding = ListItemBankBinding.inflate(inflater, parent, false)
                BankViewHolder(binding)
            }
            else -> throw NotImplementedError("Unknown View Type")
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val obj = list[position]
        when (getItemViewType(position)) {
            HEADER -> (holder as HeaderViewHolder).format(obj as String)
            ITEM -> (holder as BankViewHolder).format(obj as Bank, listener)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (list[position]) {
            is String -> HEADER
            else -> ITEM
        }
    }

    internal fun setData(data: List<Bank>) {
        val list = mutableListOf<Any>()

        list.add("Principais bancos")
        list.addAll(data.filter {
            it.code == "001"
        })
        list.add("Todos bancos")
        list.addAll(data.filter {
            it.code != "001"
        })

        this.list = list
        notifyDataSetChanged()
    }

    object ContactAdapterViewType {
        const val HEADER = 11
        const val ITEM = 22
    }

}