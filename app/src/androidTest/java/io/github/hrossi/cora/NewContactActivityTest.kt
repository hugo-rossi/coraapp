package io.github.hrossi.cora

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import io.github.hrossi.cora.presentation.contact.neww.NewContactActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NewContactActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(NewContactActivity::class.java)

    @Test
    fun shouldRegisterNewContact() {
        onView(withId(R.id.editText)).perform(typeText("111"))
        onView(withText("Próximo")).check(matches(not(isEnabled())))
        onView(withId(R.id.editText)).perform(typeText("11111111"))
        onView(withText("Próximo")).check(matches(isEnabled()))
        onView(withText("Próximo")).perform(click())

        onView(withId(R.id.editText)).perform(typeText("Jonh "))
        onView(withId(R.id.editText)).perform(clearText())
        onView(withText("Próximo")).check(matches(not(isEnabled())))
        onView(withId(R.id.editText)).perform(typeText("John Doe"))
        onView(withText("Próximo")).check(matches(isEnabled()))
        onView(withText("Próximo")).perform(click())

        onView(withText("Banco do Brasil")).perform(click())

        onView(withId(R.id.editText)).perform(typeText("00"))
        onView(withText("Próximo")).check(matches(not(isEnabled())))
        onView(withId(R.id.editText)).perform(typeText("01"))
        onView(withText("Próximo")).check(matches(isEnabled()))
        onView(withText("Próximo")).perform(click())

        onView(withId(R.id.editText)).perform(typeText("001"))
        onView(withId(R.id.editText)).perform(clearText())
        onView(withId(R.id.editText)).perform(typeText("00257130"))
        onView(withText("Concluir cadastro")).check(matches(isEnabled()))
        onView(withText("Concluir cadastro")).perform(click())
    }

}