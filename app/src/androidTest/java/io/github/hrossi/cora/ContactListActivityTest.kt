package io.github.hrossi.cora

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import io.github.hrossi.cora.presentation.contact.list.ContactListActivity
import org.hamcrest.Matchers.not

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule

@RunWith(AndroidJUnit4::class)
class ContactListActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(ContactListActivity::class.java)

    fun checkRegisterContactButton() {
        onView(withText("Cadastrar novo contato")).perform(click())
    }

    @Test
    fun checkContactListComponents() {
        onView(withText("Meus contatos")).check(matches(isDisplayed()))
        onView(withText("Cadastrar novo contato")).check(matches(isDisplayed()))
        onView(withText("Contatos cadastrados")).check(matches(isDisplayed()))
        onView(withText("Angel Flores")).check(matches(isDisplayed()))
        onView(withText("123.456.789-09")).check(matches(isDisplayed()))
    }

    @Test
    fun checkContactToastOnListClick() {
        onView(withText("Angel Flores")).perform(click())
        onView(withText("OnClick -> Angel Flores"))
            .inRoot(withDecorView(not(activityRule.activity.window.decorView)))
            .check(matches(isDisplayed()))
    }

}