# Cora App

Projeto desenvolvido como parte do processo seletivo da [Cora](https://www.cora.com.br)

Koin foi a ferramenta para injeção de dependencias utilizada e seus módulos podem ser encontrados no seguinte [package](app/src/main/java/io/github/hrossi/cora/common/di).

Room foi utilizado para persistencia de dados. Uma pequena carga de dados é realizada no seguinte [arquivo](app/src/main/java/io/github/hrossi/cora/common/di/DatabaseModule.kt).

De maneira resumida, existem basicamente 2 activities:
[ContactListActivity.kt](app/src/main/java/io/github/hrossi/cora/presentation/contact/list/ContactListActivity.kt)
[NewContactActivity.kt](app/src/main/java/io/github/hrossi/cora/presentation/contact/neww/NewContactActivity.kt)

Foi criada uma [Custom View](app/src/main/java/io/github/hrossi/cora/presentation/custom/InputView.kt) para input de texto.
- Title - Titulo do componente
- Hint - Dica que é exibida enquanto o usuário não digitou nada
- Mask - Mascara, preparada para números apenas
- Regex - Regex para validação dos dados informados - Essa abordagem foi escolhida para o teste mas poderia ser feito de outras maneiras.



Principais componentes utilizados:

- Data Binding
- Live Data
- Navigation
- View Model
- Room

Injeção de dependência:

- Koin